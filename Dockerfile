FROM debian:bullseye

# Dependencies for Olaris
RUN apt-get -y update && \
	apt-get install -y --no-install-recommends curl ca-certificates && \
	apt-get autoremove && \
	apt-get clean

# Copy the binary into the image
COPY olaris /usr/local/bin/olaris
COPY rclone /usr/local/bin/rclone

ENV XDG_CACHE_HOME /cache
EXPOSE 8080
CMD ["olaris"]
